import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import load_iris
from sklearn.preprocessing import LabelEncoder
encoder = LabelEncoder()
from sklearn.model_selection import train_test_split
import streamlit as st
from sklearn.metrics import f1_score

st.title('Classification Model for Flower Species')
data = load_iris()
df = pd.DataFrame(data.data,columns=['f1','f2','f3','f4'])
target = data.target
data_to_show = df
data_to_show['Class_label'] = target
st.subheader('Raw data')
st.write(data_to_show)

X_train, X_test, y_train, y_test = train_test_split(df,target,test_size = 0.20,random_state = 42)
model = RandomForestClassifier()
model.fit(X_train,y_train)

pred_labels = model.predict(X_test)

print("-----Performance_score_is---------")
print("The f1 score is:{}".format(f1_score(y_test,pred_labels,average='weighted')))

st.subheader('The accuracy of model is:')
st.write(f1_score(y_test,pred_labels,average='weighted'))
print("----------------")

